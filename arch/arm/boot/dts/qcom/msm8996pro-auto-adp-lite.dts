/* Copyright (c) 2015-2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/dts-v1/;

#include <dt-bindings/gpio/gpio.h>
#include "msm8996pro.dtsi"
#include "msm8996-pm8994.dtsi"
#include "msm8996-agave-adp.dtsi"
#include "msm8996pro-auto.dtsi"

/ {
	model = "Qualcomm Technologies, Inc. MSM 8996pro AUTO ADP LITE";
	compatible = "qcom,msm8996-adp", "qcom,msm8996", "qcom,adp";
	qcom,msm-id = <315 0x10000>;
	qcom,board-id = <0x03010019 0>;
};

&spi_9 {
	status = "disabled";
};

&soc {
	qcom,msm-thermal {
		qcom,hotplug-temp = <115>;
		qcom,hotplug-temp-hysteresis = <25>;
		qcom,therm-reset-temp = <119>;
	};

	i2c@75b6000 { /* BLSP8 */
		/* ADV7533 HDMI Bridge Chip removed on ADP Lite */
		adv7533@39 {
			status = "disabled";
		};
	};

	qcom,adv7481@70 {
		status = "disabled";
	};

	qcom,msm-ba {
		status = "disabled";
	};

	qcom,tv-tuner {
		status = "disabled";
	};
};

&dsi_adv_7533_2 {
	/delete-property/ qcom,dsi-display-active;
};

&sde_kms {
	connectors = <&sde_hdmi_tx &sde_hdmi &dsi_adv_7533_1>;
};

&pil_modem {
	pinctrl-names = "default";
	pinctrl-0 = <&modem_mux>;
};

&slim_msm {
	status = "disabled";
};

&pm8994_mpps {
	mpp@a500 { /* MPP 6 */
		qcom,mode = <1>;		/* Digital output */
		qcom,output-type = <0>;		/* CMOS logic */
		qcom,vin-sel = <2>;		/* S4 1.8V */
		qcom,src-sel = <0>;		/* Constant */
		qcom,master-en = <1>;		/* Enable GPIO */
		status = "okay";
	};
};

&sdhc_2 {
	cd-gpios = <&tlmm 38 GPIO_ACTIVE_LOW>;
	pinctrl-0 = <&sdc2_clk_on &sdc2_cmd_on &sdc2_data_on &sdc2_cd_on_sbc>;
	pinctrl-1 = <&sdc2_clk_off &sdc2_cmd_off &sdc2_data_off
			&sdc2_cd_on_sbc>;
};

&i2c_7 {
	silabs4705@11 { /* SiLabs FM chip, slave id 0x11*/
		status = "disabled";
	};
};

&aliases {
	mhi1 = &mhi;
	mhi_uci0 = &mhi_uci;
};

&mhi {
	compatible = "qcom,mhi";
	qcom,pci-dev_id = <0x1102>;
	qcom,pci-domain = <0>;
	qcom,pci-bus = <1>;
	qcom,pci-slot = <0>;
	qcom,mhi-address-window = <0x0 0x80000000 0x1 0xFFFFFFFF>;

	/* RUMI Specific param */
	qcom,mhi-ready-timeout = <600000>;
	qcom,bhi-poll-timeout = <600000>;
	qcom,bhi-alignment = <0x40000>;

	/* firmware transfer */
	qcom,mhi-manage-boot;
	qcom,mhi-fw-image = "amss.bin";
	qcom,mhi-max-sbl = <0x40000>;
	qcom,mhi-sg-size = <0x80000>;
	qcom,emulation;

	/* mhi cntxt parameters */
	mhi-chan-cfg-0 = <0x0 0x80 0x1 0x92>;
	mhi-chan-cfg-1 = <0x1 0x80 0x1 0xa2>;
	mhi-chan-cfg-4 = <0x4 0x80 0x1 0x92>;
	mhi-chan-cfg-5 = <0x5 0x80 0x1 0xa2>;
	mhi-chan-cfg-16 = <0x10 0x40 0x1 0x92>;
	mhi-chan-cfg-17 = <0x11 0x40 0x1 0xa2>;
	mhi-event-rings = <2>;
	mhi-event-cfg-0 = <0xa 0x0 0x1 0 1 0x31>;
	mhi-event-cfg-1 = <0x80 0x1 0x1 0 1 0x31>;

	status = "okay";
};

&mhi_uci {
	status = "ok";
};

&ipc_rtr_mhi {
	qcom,out-chan-id = <16>;
	qcom,in-chan-id = <17>;
	qcom,mhi = <&mhi>;
};

&diag_mhi {
	qcom,mhi = <&mhi>;
	status = "disabled";
};

&diag_hsic {
	status = "disabled";
};

&diag_sdio {
	status = "ok";
};

&qcn_sdio {
	status = "okay";
};

&sdio_bridge_tty {
	status = "okay";
};

&sdio_bridge_ipc {
	status = "okay";
};

&sdio_bridge_diag {
	status = "okay";
};

&pcie0 {
	qcom,l1-supported;
	qcom,l1ss-supported;
};
